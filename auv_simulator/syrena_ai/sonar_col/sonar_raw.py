import cv2
import numpy as np
from matplotlib import pyplot as plt

import dateutil.parser

import glob2
import xml.etree.ElementTree
import os.path

import imutils



def merge_two_dicts(x, y):
    """Given two dicts, merge them into a new dict as a shallow copy."""
    z = x.copy()
    z.update(y)
    return z


def get_sonar_data(path, start=None, limit=None):

    e = xml.etree.ElementTree.parse(path + "/meta.xml").getroot()

    i = 0 

    for ping in e.findall('ping'):

        i+=1

        if limit and start:
            if i< start: continue

            if i >= limit+start: break
        
        
        img = ping[0]

        output = merge_two_dicts(ping.attrib, img.attrib)

        output['file'] = path + "/" + img.text
        output['time'] = dateutil.parser.parse(output['time'])

        for attr in ['depth', 'head', 'origin_col', 'number', 'origin_row', 'width', 'utcoffset', 'height']:
            output[attr] = int(output[attr])

        output["range_res"] = float(output["range_res"])
        output['image'] = read_raw_sonar(output)


        output['origin'] = (output['origin_row'], output['origin_col'])

        yield output



def read_raw_sonar(ping):

    image_file = ".".join(ping['file'].split('.')[:-1])+".png"

    if os.path.isfile(image_file):
        # print "c",
        img = cv2.imread(image_file)[:,:,1]
        return img

    with open(ping['file'], mode='r') as f:
        a = (np.fromfile(f, dtype='>u2')) # / 65535.0 * 255
        a = log_scale(a)
        b = np.reshape(a, (ping['height'], ping['width']))

    cv2.imwrite(image_file,b.astype(np.uint8))
    return b


def cart_to_polar_remap(ping):

    theta_res = 10.0
    theta_angle = 130.0
    theta_px = theta_angle * theta_res + 1
    theta_shift = (90 - theta_angle / 2)

    lower_R = 20
    upper_R = ping['height']-(0.6 / ping["range_res"])-100
    r, theta = np.mgrid[lower_R:upper_R, :theta_px]

    cx, cy = ping['origin'][::-1]

    theta = (theta - (theta_angle + theta_shift) * theta_res) / theta_res

    r = ping['height'] - r - 1

    # print "theta : ", cv2.minMaxLoc(theta)

    x = np.cos(np.radians(theta)) * r + cx

    y = np.sin(np.radians(theta)) * r + cy

    return theta_angle/theta_px,cv2.remap(ping['image'], x.astype('float32'), y.astype('float32'), cv2.INTER_AREA)


def cart_to_polar(ping):

    theta_angle = 130.0


    # print origin

    origin = ping['origin'][::-1]

    # origin = 200,100

    rad = ping['height']

    # print(dir(cv2))
    # 0cv2.WARP_FILL_OUTLIERS)

    img = ping['image']

    img = np.float32(img)
    img = cv2.linearPolar(ping['image'], origin, rad, 0)


    lim = int((img.shape[0]*(1-theta_angle/180))//2)
    img = img[img.shape[0] / 2 + lim:-lim, 40:-20:].T


    img = cv2.resize(img,None,fx=5, fy=0.5, interpolation = cv2.INTER_LINEAR)

    # print cv2.minMaxLoc(img)

    # img = np.log(img+1)
    # ret,img = cv2.threshold(img,0,0.7,cv2.THRESH_BINARY)
    # return 0,
    return 0,img#img[::-1,]

def on_mouse(event, x, y, flag, param):
    if(event == cv2.EVENT_LBUTTONDOWN):
        # print param
        print x,y

def log_scale(img):

    base = np.e ** (np.log(65535)/255)


    img = img.astype(np.float64)
    img = np.log(img+1)/np.log(base)

    print cv2.minMaxLoc(img)

    if img.min() != 0.0:
        raise ValueError("img contain none 0.0 minima",str(cv2.minMaxLoc(img)))
    # if cv2.minMaxLoc(b)


    return img.astype(np.uint8)

def nothing(test):
    pass

if __name__ == '__main__':

    path = "/home/universez/Desktop/sonar_col/exported/slam_middle_kuan/"

    img = None

    for ping in get_sonar_data(path):
        # print ping['image']

        img = ping['image']

        break
        # continue

        # img = img * 65535.0 / 255

        # np.savetxt('hist_raw.out', img.ravel(),fmt='%d') ; exit()

        # x,y = 0,100; cv2.rectangle(img, (x, y), (x+20, y+20), (255,0,0), 3);
        # x,y = ping['width']-100,100; cv2.rectangle(img, (x, y), (x+20, y+20), (150,0,0), 6);
        # x,y = ping['width']-100,ping['height']-100; cv2.rectangle(img, (x, y), (x+20, y+20), (255,0,0), 9)
        # x,y = 0,ping['height']-100; cv2.rectangle(img, (x, y), (x+20, y+20), (255,0,0), 12)
        # print ping


        # log_img = log
        # cv2.imshow('log', log_scale(img))
        cv2.imshow('raw', img)

        threshold = 75

        idx = img[:,:] <= threshold
        img[idx] = 0

        cv2.imshow('thred', img)

        plt.hist(img.ravel(),256,[50,256]); plt.show()

        # print img.dtype
        # print cv2.minMaxLoc(img)
        # cv2.imshow('uint16', img.astype(np.uint16))
        # cv2.imshow('uint8', img.astype(np.uint8))

        # img_float = (img/65535.0* 255)
        # # cv2.imshow('raw', img/ 65535.0 * 255)
        # print img_float.dtype
        # print img_float
        # print cv2.minMaxLoc(img_float)
        # cv2.imshow("img_float"+str(img_float.dtype), img_float)

        if cv2.waitKey(0) == 27:
            break
        # cv2.waitKey()
        exit()

    cv2.namedWindow('image')

    # create trackbars for color change
    cv2.createTrackbar('threshold','image',0,255,nothing)
    cv2.setTrackbarPos('threshold','image',138)

    cv2.createTrackbar('alpha','image',0,300,nothing)
    cv2.setTrackbarPos('alpha','image',100)


    cv2.createTrackbar('beta','image',0,255,nothing)


    

    while 1:
        
        for ping in get_sonar_data(path):
        # print ping['image']

            img = ping['image']
            img = imutils.resize(img, width=920).astype(np.uint8)

            img_ = np.float32(img)

            threshold = cv2.getTrackbarPos('threshold','image')
            alpha = cv2.getTrackbarPos('alpha','image')
            beta = cv2.getTrackbarPos('beta','image')

            idx = img_[:,:] <= threshold
            img_[idx] = 0

            idx = img_[:,:] > threshold 
            img_[idx] *= (alpha/100.0 )
            img_[idx] += beta



            # img_ += beta
            # idx = img_[:,:] > threshold 
            # img_[idx] *= (1 + intensity/100.0 )

            idx = img_[:,:] > 255
            img_[idx] = 255

            img_ = (img_).astype(np.uint8)
            img_ = cv2.equalizeHist(img_)

            show_img = img_
            
            cv2.imshow('image',np.hstack((img,show_img)))
            
            k = cv2.waitKey(1) & 0xFF


            if k == 32:
                cv2.waitKey(0)
            # if k == 20:
                
            if k == 27:
                cv2.destroyAllWindows()
                exit()
                break

        # cv2.waitKey(0)

        # get current positions of four trackbars


        # d1-d2.total_seconds()

        # main()