import cv2
import numpy as np

import matplotlib.pyplot as plt

from sonar_raw import *

def hist_match(source, template):
    """
    Adjust the pixel values of a grayscale image such that its histogram
    matches that of a target image

    Arguments:
    -----------
        source: np.ndarray
            Image to transform; the histogram is computed over the flattened
            array
        template: np.ndarray
            Template image; can have different dimensions to source
    Returns:
    -----------
        matched: np.ndarray
            The transformed output image
    """

    oldshape = source.shape
    source = source.ravel()
    template = template.ravel()

    # get the set of unique pixel values and their corresponding indices and
    # counts
    s_values, bin_idx, s_counts = np.unique(source, return_inverse=True,
                                            return_counts=True)
    t_values, t_counts = np.unique(template, return_counts=True)

    # take the cumsum of the counts and normalize by the number of pixels to
    # get the empirical cumulative distribution functions for the source and
    # template images (maps pixel value --> quantile)
    s_quantiles = np.cumsum(s_counts).astype(np.float64)
    s_quantiles /= s_quantiles[-1]
    t_quantiles = np.cumsum(t_counts).astype(np.float64)
    t_quantiles /= t_quantiles[-1]

    # interpolate linearly to find the pixel values in the template image
    # that correspond most closely to the quantiles in the source image
    interp_t_values = np.interp(s_quantiles, t_quantiles, t_values)

    return interp_t_values[bin_idx].reshape(oldshape)


capture = None
def get_next_video():
    

    ret, img = capture.read()

    mask = np.zeros((img.shape[0], img.shape[1]))
    cv2.fillConvexPoly(mask, pts, 1)
    mask = mask.astype(np.bool)
    out = np.zeros_like(img)
    return img[mask]

if __name__ == '__main__':

    path = "/home/universez/Desktop/sonar_col/exported/slam_middle_kuan/"
    
    
    pts = np.array([[61, 63], [447, 63], [450, 326],
                    [55, 326]], dtype=np.int32)
    capture = cv2.VideoCapture("sonar_polar.avi")

    # cv2.imshow("get_next_video",get_next_video())
    
    # template = np.loadtxt('hist_avi.out')#,fmt='%d')

    # print template.shape

    x,y = 0,0
    data_x = []
    data_y = []

    limit = 100

    start = None#270

    last_polar = None

    for ping in get_sonar_data(path,start,limit):

        # template = get_next_video()
                
        img = ping['image']


        # x,y = 0,100; cv2.rectangle(img, (x, y), (x+20, y+20), (255,0,0), 3);
        # x,y = ping['width']-100,100; cv2.rectangle(img, (x, y), (x+20, y+20), (150,0,0), 6);
        # x,y = ping['width']-100,ping['height']-100; cv2.rectangle(img, (x, y), (x+20, y+20), (255,0,0), 9)
        # x,y = 0,ping['height']-100; cv2.rectangle(img, (x, y), (x+20, y+20), (255,0,0), 12)
        # print ping


        polar = img
        # theta_res,polar = cart_to_polar(ping)

        
        # theta_res,polar = cart_to_polar_remap(ping)

        polar = np.float32(polar)
        
        threshold = 138
        
        idx = polar[:,:] <= threshold
        polar[idx] = 0

        idx = polar[:,:] > threshold 
        polar[idx] *= 1

        idx = polar[:,:] > 255
        polar[idx] = 255


        # polar = hist_match(polar,template)/256.0
        # print polar.max()

        

        # polar = cv2.GaussianBlur(polar,(27,27),1)
        polar = (polar).astype(np.uint8)
        polar = cv2.equalizeHist(polar)

        polar = np.float32(polar)

        if True:
            # cv2.imshow('raw', img)
            cv2.imshow('polar', polar/255.0)

            # cv2.waitKey(100)
        
        # polar = np.log(polar+1)

        
        # polar = np.

        # print cv2.minMaxLoc(polar)

        # pts = np.array([[1, 1], [1, polar.shape[1]-1], [polar.shape[0]-1, polar.shape[1]-1],
        #             [polar.shape[0]-1, 1]], dtype=np.int32)
        # pts = np.array([[0, 0], [0,100],[100,100],[100,0]], dtype=np.int32)

        # mask = np.zeros((polar.shape[0], polar.shape[1]))

        # cv2.fillConvexPoly(mask, pts, 1)
        # mask = mask.astype(np.bool)
        # out = np.zeros_like(polar)

        # polar = polar[mask]



        if last_polar is not None:
            d = cv2.phaseCorrelate(last_polar, polar)

            # if d[-1] < 0.8: print d[-1]
            dx, dy = d[0]

            mid_shape_y = polar.shape[1]/2.0


            if abs(dy) >= 50:#mid_shape_y/2:
                print "><"*50,dy

                dy = 0


            # print dy,mid_shape_y
            # while dy > mid_shape_y:
            #     dy -= mid_shape_y
                
            # while dy < -mid_shape_y:
            #     dy += mid_shape_y

                
            #     # print polar.shape[1]
            #     dy = 0

            # print i,dx, dy, d[-1]
            print "%d %7.4f %7.4f %7.4f"%(0,dx,dy,d[-1])#*0.3217821782178218
            x += dx
            y += dy# * theta_res  # *FRAME_RATE

            data_x += [x]
            data_y += [y]

        last_polar =  np.copy(polar)


        # print
        # cv2.setMouseCallback("polar",on_mouse,param=None)


        if 0xFF & cv2.waitKey(1) == 27:
            break
        # cv2.waitKey()
        # exit()

    # d1-d2.total_seconds()
    cv2.destroyAllWindows()

    plt.scatter(data_y, range(len(data_y)))

    plt.grid(True)

    plt.axes().set_aspect('equal', 'datalim')

    plt.show()

    # main()
