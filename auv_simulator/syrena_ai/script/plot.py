import matplotlib.pyplot as plt
import numpy as np


class Plot(object):
    def __init__(self, dimension=11, n=1, lim_y=None ,lim_x=None, text="",disable=False,projection=None):

        self.disable = disable


        if self.disable: return

        self.fig = plt.figure()

        self.dimension = dimension * 10 + 1

        self.n = n

        self.lim_y = lim_y


        self.lim_x = lim_x

        plt.subplots_adjust(hspace=.001)

        if text:
            self.fig.suptitle(text)

        self.ax = []
        self.li = []

        for i in xrange(n):

            #print dimension
            ax = self.fig.add_subplot(str(self.dimension + i),projection=projection)
            self.ax.append(ax)

            if self.lim_y:
                self.set_ylim(self.lim_y, i)

            if self.lim_x:
                self.set_xlim(self.lim_x, i)

            li, = self.ax[i].plot([0], [0], marker='.', linestyle='none')

            self.li.append(li)

        #self.li, = self.ax.plot([0], [0],marker='o', linestyle='none')

        plt.show(block=False)

    def set_title(self, text, i=0):
        if self.disable: return

        if self.ax[i].get_title != text:
            self.ax[i].set_title(text)

    def set_ylim_all(self, lim_y):
        if self.disable: return

        for i in xrange(self.n):
            self.set_ylim(lim_y, i)

    def set_ylim(self, lim_y, i=0):
        if self.disable: return

        self.ax[i].set_ylim(*(lim_y))

    def set_xlim(self, lim_x, i=0):
        if self.disable: return

        self.ax[i].set_xlim(*(lim_x))

    def clear_data(self, i=0):
        if self.disable: return

        self.li[i].set_xdata([0])
        self.li[i].set_ydata([0])
        self.set_title("",i)

    def update(self, data, y=None, i=0, text=None, lim_x=None, lim_y=None):
        if self.disable: return None,None

        if y is not None:
            x = data
            y = y
        else:
            x, y = data.T

        if text is not None:
            self.set_title(text, i)

        if self.lim_x:
            pass
        elif lim_x is None:
            lim_x = (x.min(), x.max())
            self.set_xlim(lim_x, i)
        else:
            self.set_xlim(lim_x, i)

        if self.lim_y:
            pass
        elif lim_y is None:
            lim_y = (y.min(), y.max())
            self.set_ylim(lim_y, i)
        else:
            self.set_ylim(lim_y, i)

        self.li[i].set_xdata(x)
        self.li[i].set_ydata(y)

        self.fig.canvas.draw()

        return lim_x, lim_y
        # except:
        #     raise ValueError(len(x),"!=",len(y))
