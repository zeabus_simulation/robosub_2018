
import tf
from math import *
from geometry_msgs.msg import Twist

def pprint(l):
    
    type_ = type(l)
    if type_ == list:
        for i in l:
            print "%8.3f"%(i),
        print
        
        return
        
def ttl(a): #a is twist
    tmp=[0,0,0,0,0,0]
    tmp[0]=a.linear.x
    tmp[1]=a.linear.y
    tmp[2]=a.linear.z
    tmp[3]=a.angular.x
    tmp[4]=a.angular.y
    tmp[5]=a.angular.z
    return tmp

def ltt(a): #a is list
    tmp=Twist()
    tmp.linear.x=a[0]
    tmp.linear.y=a[1]
    tmp.linear.z=a[2]
    tmp.angular.x=a[3]
    tmp.angular.y=a[4]
    tmp.angular.z=a[5]
    return tmp

def diffAng(a,b):

    a,b = [ (ang+4*pi) % (2*pi) for ang in [a,b]]
    
    diff = a - b
    
    if diff >  pi: diff -= 2*pi 
    elif diff < -pi: diff += 2*pi 
    
    return diff


def euler_from_quaternion(r):
    tmp=(r.x,r.y,r.z,r.w)
    ang=tf.transformations.euler_from_quaternion(tmp)
    return tf.transformations.euler_from_quaternion(tmp)



