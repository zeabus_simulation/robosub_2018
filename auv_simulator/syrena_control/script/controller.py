#!/usr/bin/env python

import rospy
from math import *
from SPID import SPID

from nav_msgs.msg import Odometry
from std_msgs.msg import Float64
from geometry_msgs.msg import Twist

from PID_reconfigure import PID_reconfigure
import rospkg
from support import *
from time import time


class Controller():
    def __init__(self, file_name, node="PID"):

        self.HZ = 50.0

        self.pose = [0] * 6
        self.v = [0] * 6

        self.isFixPoint = [0, 0, 0, 1, 1, 0]
        self.lastPoint = [0, 0, 0, 0.08046, 0, 0]

        self.target_v = [0] * 6

        self.fix_xy = True

        self.cmd = [0] * 6
        self.en = False

        self.pidP = [SPID(0, 0, 0, self.HZ * 2) for i in xrange(6)]
        self.pidV = [SPID(1, 0, 0, self.HZ * 2) for i in xrange(6)]

        rospy.init_node(node)
        PID = PID_reconfigure(file_name, node, self.pidP, self.pidV)

        self.Pub = rospy.Publisher("/syrena/cmd_vel", Twist, queue_size=10)
        #rospy.Subscriber('/syrena/state', Odometry, self.updState)
        rospy.Subscriber('/auv/state',Odometry,self.updState)

        rospy.Subscriber('/cmd_vel/smoothed', Twist, self.update_sim)

        self.setup_fix()
        print 'init complete'

    def updState(self, res):

        pose = res.pose.pose
        self.v = ttl(res.twist.twist)
        ang = euler_from_quaternion(pose.orientation)
        self.pose[0] = pose.position.x
        self.pose[1] = pose.position.y
        self.pose[2] = pose.position.z
        self.pose[3] = ang[0]
        self.pose[4] = ang[1]
        self.pose[5] = ang[2]

        self.en = True

    def update_sim(self, twist):

        cmd_temp = ttl(twist)

        if self.en is not True:
            return

        if self.fix_xy:
            if cmd_temp[0:2] == [0, 0]:
                if self.isFixPoint[0:2] == [0, 0]:
                    self.lastPoint[0:2] = self.pose[0:2]
                self.isFixPoint[0:2] = [1, 1]
            else:
                self.isFixPoint[0:2] = [0, 0]

        for i in xrange(2, 6):
            if cmd_temp[i] == 0:
                if not self.isFixPoint[i]:
                    self.lastPoint[i] = self.pose[i]
                self.isFixPoint[i] = 1

            else:
                self.isFixPoint[i] = 0

        self.cmd = list(cmd_temp)
        self.target_v = list(cmd_temp)

    def calXY(self):

        if self.isFixPoint[0:2] == [1, 1]:
            v = self.xy_direct(
                self.lastPoint[0] - self.pose[0], self.lastPoint[1] - self.pose[1])
            for i in xrange(2):
                self.cmd[i] = self.pidP[i].pid(-v[i])

    def calPID(self):
        for i in xrange(6):
            if self.isFixPoint[i]:
                if i > 2:
                    self.cmd[i] = self.pidP[i].pid(
                        diffAng(self.lastPoint[i], self.pose[i]))
                elif i == 2:
                    self.cmd[i] = self.pidP[i].pid(
                        self.lastPoint[i] - self.pose[i])
            else:
                self.cmd[i] = self.target_v[i] + \
                    self.pidV[i].pid(self.target_v[i] - self.v[i])

    def get_deltatime(self):
        temp, self.time = self.time, time()

        return self.time - temp

    def run(self):
        rate = rospy.Rate(self.HZ)

        rospy.sleep(0.3)

        self.time = time()
        self.time_start = time()
        self.last_plot = []

        times = []

        sum_times = 0.0

        while not rospy.is_shutdown():

            self.calPID()
            self.calXY()

            cmd_tmp = ltt(self.cmd)

            print "#Fix"
            pprint(self.isFixPoint)

            print "#state"
            pprint(self.pose)

            print "#last_point"
            pprint(self.lastPoint)

            print "#target_v"
            pprint(self.target_v)

            print "#vel"

            pprint(self.v)

            print "#publish"

            pprint(ttl(cmd_tmp))

            print
            # print "PID"

            # for p, v in zip(self.pidP, self.pidV):
            #     print(p.get(), v.get())

            print "time per loop : %.4f s (%d)" % (sum_times,len(times))
            print "=="*20

            self.Pub.publish(cmd_tmp)

            time_from_start = time() - self.time_start

            dt = self.get_deltatime()
            sum_times += dt/self.HZ
            times.append(dt)
            if len(times) > self.HZ:
                dt = times.pop(0)
                sum_times -= dt/self.HZ

            rate.sleep()

    def fix_rel(self, resp, index):

        resp.data += self.pose[index]
        self.fix_abs(resp, index)

    def fix_abs(self, resp, index):

        if index > 2:  # for angle
            resp.data = (resp.data + 4 * pi) % (2 * pi)

        self.isFixPoint[index] = 1
        self.lastPoint[index] = resp.data

    def xy_direct(self, x, y):
        theta = pi - self.pose[-1]
        s = sin(theta)
        c = cos(theta)

        return x * c - y * s, x * s + y * c

    def setup_fix(self):

        axises = {"x": 0, "y": 1, "depth": 2, "yaw": 5}

        for axis in axises:
            rospy.Subscriber('/fix/abs/' + axis, Float64,
                             self.fix_abs, axises[axis])
            rospy.Subscriber('/fix/rel/' + axis, Float64,
                             self.fix_rel, axises[axis])


if __name__ == '__main__':

    file_name = rospkg.RosPack().get_path('syrena_control') + "/param/PID.yaml"
    x = Controller(file_name=file_name)
    x.run()
    rospy.spin()
