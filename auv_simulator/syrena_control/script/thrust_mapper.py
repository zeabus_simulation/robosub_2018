#!/usr/bin/env python

import rospy
#from modbus_ascii_ros.msg import Pwm
from geometry_msgs.msg import *
from sensor_msgs.msg import Joy
from numpy  import *
from sensor_msgs.msg import JointState

"""
t = [t0, t1, t2, t3, t4, t5, t6, t7]
  """

pwm_publisher = 0
d = 0 #d = direction of each thruster
r = 0 #r = distance from origin to each thruster
T = 0 #T = matrix map thruster forces to robot force
M = 0 #M = matrix map robot force to thruster forces


max_thruster_force_pos = 51
max_thruster_force_neg = 41

body_radius = 0.11
t_radius = 0.04575


t_lr_offset = 0.07
t_fb_offset = 0.07


t_lr = t_lr_offset + t_radius
t_fb = t_fb_offset + t_radius


body_length = 1.2

T_lr = body_radius+t_lr
T_fb = body_length/2 + t_fb

def print_for_sim(TT):
	print "\n========T for sim=========\n"


	for i in xrange(len(TT)):
		T = TT[i]
		T = " ".join(map(str,T))
		print \
"""<thruster>
	<name>T%d</name>
	<map>%s</map>
	<effort>%.2f</effort>
</thruster>"""%(i,T,max_thruster_force_pos)

	print "\n===========================\n"
	for i in xrange(len(TT)):
		print \
"""<joint name="T%d">
	<limit lower="-%.2f" upper="%.2f"/>
</joint>
"""%(i,max_thruster_force_neg,max_thruster_force_pos)

class thrust_mapper:
	def __init__(self):
		rospy.init_node('thrust_mapper')
		#	self.pwm_publisher = rospy.Publisher('pwm', Pwm, queue_size=10)
		self.sim_publisher = rospy.Publisher('/syrena/body_command', JointState, queue_size=10)

		rospy.Subscriber("/syrena/cmd_vel", Twist, self.twist_callback)


		self.number_thruster = 8
		#d = direction of each thruster  [0,2,5,6 reverse direction]
		print '============d============='
		self.d = array([
			   [1, 0, 0],  # L
			   [1, 0, 0],  # R
			   [0, 1, 0],  # F
			   [0, -1,0],  # B
			   [0, 0, 1],  # FL
			   [0, 0, 1],  # FR
			   [0, 0, 1],  # BL
			   [0, 0, 1],
			   ]) # BR

		print self.d


		# r = distance from origin to each thruster
		print '\n============r============='
		self.r = array([
			   [0, -T_lr, 0],
			   [0, +T_lr, 0],
			   [+T_fb, 0, 0],
			   [-T_fb, 0, 0],
			   [+T_fb, -T_lr, 0],
			   [+T_fb, +T_lr, 0],
			   [-T_fb, -T_lr, 0],
			   [-T_fb, +T_lr, 0],
			   ])

		print self.r

		#T = matrix map thruster forces to robot force
		#T = [6 x 8]
		print '\n============T============='
		self.T_tmp = array([
			   cross(self.r[0].T, self.d[0].T),
			   cross(self.r[1].T, self.d[1].T),
			   cross(self.r[2].T, self.d[2].T),
			   cross(self.r[3].T, self.d[3].T),
			   cross(self.r[4].T, self.d[4].T),
			   cross(self.r[5].T, self.d[5].T),
			   cross(self.r[6].T, self.d[6].T),
			   cross(self.r[7].T, self.d[7].T)
			   ])

		#self.T_tmp = array(list(self.T_tmp)[0:use])
		print self.T_tmp
		print

		#T = matrix map thruster forces to robot force
		#T = [6 x 8]

		self.T = concatenate((self.d.T, self.T_tmp.T))
		print self.T

		## for sim
		#print_for_sim(self.T.T)

		#print self.T.T

		#M = matrix map robot force to thruster forces
		#M = [8 x 6]
		print '\n============M============='
		self.M = linalg.pinv(self.T)
		print self.M.shape
		print
		print self.M



		print '\n============Mul_M============='

		self.mul_m = [[i for i in l if abs(i)>=0.00001] for l in list(self.M.T)]

		self.mul_m = map(lambda x:1.0/max(x),self.mul_m)


		#self.mul_m[-1]-=2
		print self.mul_m

		self.output = JointState()
		self.output.name = [ "T%d"%i for i in xrange(self.number_thruster)]


	def bound(self,cmd):
		x = []
		for i in cmd:
			if i > max_thruster_force_pos:
				x.append(max_thruster_force_pos)
			elif i < -max_thruster_force_neg:
				x.append(-max_thruster_force_neg)
			else:
				x.append(i)
		return x

	def twist_callback(self , message):
		#print '** input ** '
		#print message

		#compute thrust for each thruster based on joy stick command

		twist_list = [message.linear.x, message.linear.y, message.linear.z,
			   message.angular.x, message.angular.y, message.angular.z]


		F = array(map(lambda x,y:x*y,twist_list,self.mul_m))
		#print F

		t = dot(self.M, F.T)
		#print '** thrust **'
		#print t
		cmd = [ (max_thruster_force_pos if i >0 else max_thruster_force_neg)*i  for i in t ]

		tmp = self.bound(cmd)

		output = JointState()


		#output.name = [ "T%d"%i for i in xrange(self.number_thruster)]
		# output.position = list(tmp)

		self.output.position = list(tmp)
		print self.output
		self.sim_publisher.publish(self.output)
		#self.pwm_publisher.publish(Pwm(tmp))

if __name__ == '__main__':
	x = thrust_mapper()
	#x.publishOdom()


	#while not rospy.is_shutdown():
		#x.twist_callback(Twist(Vector3(1,0,0),Vector3(0,0,0	)))

	#	pass
	rospy.spin()
